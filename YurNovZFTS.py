﻿""" Физтех за ноябрь"""
a = input('''Введите номер задачи (например "6"): ''')

def sortstr(a):
    '''
    Sorts string a lexicographicaly
    str a -> function(a) -> str b
    '''
    c = a
    a = list(a)
    a.sort()
    b = ''
    for i in a:
        b = b + i
    a = c
    return b

#Задача 6


def printmyage(a):
   
    '''
    1 - год;
    2,3,4 - года;
    остальное - лет;
    Используется форматируемый вывод
    '''
    a = input('Введите возраст: ')
    b = ''
    if a[-1] in ['0','5','6','7','8','9']:
        b = 'Мне %s лет'%a
        return b
    elif a[-1] == '1':
        b = 'Мне %s год'%a
        return b
    else:
        b = 'Мне %s года'%a
        return b

###Задача 7



def f14countsysToDecimal(a):
    '''Здесь простой криворукий перевод из 14-ричной СС в 10-ную СС с проверкой числа а на чётность'''
    a = input('Введите число в 14-ричной СС: ')
    b = 0
    for i in range(len(a)):
        if a[i] in ['0','1','2','3','4','5','6','7','8','9']:
            b = b + int(a[i])*pow(14,i)
        if a[i] == 'A':
            b = b + 10*pow(14,i)
        if a[i] == 'B':
            b = b + 11*pow(14,i)
        if a[i] == 'C':
            b = b + 12*pow(14,i)
        if a[i] == 'D':
            b = b + 13*pow(14,i)
    if b%2==1:
        return "YES"
    else:
        return "NO"


###Задача 8

def count_sorted_strs_LC(a):
    '''
    Переменные:
    a - входная строка
    b - число упорядоченных слов
    i - слово в списке слов строки, полученном методом сепарации по знаку запятой
    j - символ в слове i
    Алгоритм:
    Ввод a ->
    Разбиваем a на отдельные слова по запятой, выбрасывая точку ->
    Проверяем каждое слово, совпадает ли оно со своей сортированной версией
    и есть ли в ней символы не из строчной латиницы ->
    Если так - сразу переходим к следующему слову ->
    Иначе - увеличиваем b на единицу и идём к следующему слову ->
    Возврат b.
    '''
    a = input('Введите строку слов через запятую с точкой в конце строчки: ')
    a = a[:-1].split(',')
    b = 0
    k = 0
    for i in a:
        if a == '':
            break
        if i == sortstr(i):
            for j in i:
                if j not in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']:
                    k = 1
            if k != 1:
                b = b + 1
            else:
                k = 0
    return b


###Задача 9

### Написано на Pascal ABC

"""
program Nine;
var a: string;
    b,i,t,g: longint;
Begin
b := 0;
t := 0;
i := 1;
read(a);
while a <> '.' do begin
    if a = '+' then begin
        b := b + t;
        t := 0;
        i := 1;
        read(a);
        end
    else
        if a = '-' then begin
        b := b + t;
        t := 0;
        i := -1;
        read(a);
        end 
        else
        g := StrToInt64(a);
        t := t*10 + i*g;
        read(a);
        end;
writeln(b)
        
End.    
"""

### Задача 10

### Написано на Pascal ABC

"""
program Nine;
var a: string;
    b,t,g: Real;
    n,i: Int64;
Begin
b := 0;
t := 0;
i := 1;
read(a);
while a <> '.' do begin
        if a = ',' then begin
        b := b + t;
        t := 0;
        i := -1;
        read(a);
        end 
        else
        if i = 1 then begin
        g := StrToInt64(a);
        t := t*10 + g;
        read(a);
        end
        else begin
        t:= t + g/power(10;n); Inc(n); read(a);
        end
        end;
writeln(b)
        
End.        
"""

if a == '6':
    print(printmyage(a))
if a == '7':
    print(f14countsysToDecimal(a))
if a == '8':
    print(count_sorted_strs_LC(a))
    
input("Нажмите Enter, чтобы закончить работу.")
